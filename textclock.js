// JavaScript Document
function swabianClock() {

    var newDate = new Date(),
        day = newDate.getDay(),
        hours = newDate.getHours(),
        minutes = newDate.getMinutes().toString(),
        seconds = newDate.getSeconds().toString();

    if (hours > 12 && hours !== 0) {
        hours = hours - 12;
    }
    if (minutes < 10) {
        minutes = 0 + minutes;
    }
    if (seconds < 10) {
        seconds = 0 + seconds;
    }
    var minsSecs = minutes*60;
	
	//console.log(hours+':'+minutes+':'+seconds+':'+minsSecs);
	
	//Update hour
    hoursObj = {
        1: '#eins-h',
        2: '#zwei-h',
        3: '#drei-h',
        4: '#vier-h',
        5: '#fuenf-h',
        6: '#sechs-h',
        7: '#sieben-h',
        8: '#acht-h',
        9: '#neun-h',
        10: '#zehn-h',
        11: '#elf-h',
        12: '#zwoelf-h',
        0: '#midnight'
    };

	if(minutes >= 10){
		hours = hours +1;
	}
    updateHour(hoursObj[hours]);
	
	
	//Update pre hour
	 minsObj = {
        1: '#eins',
        2: '#zwei',
        3: '#drei',
        4: '#vier',
        5: '#fuenf',
        6: '#sechs',
        7: '#sieben',
        8: '#acht',
        9: '#neun',
        10: '#zehn'
    };
	
	if((minsSecs >= 0 && minsSecs < 600) || (minsSecs >= 3000 && minsSecs < 3600)){
		tmp = 15-minutes;
		if(minutes>=50){
			tmp = 60-minutes;
			console.log(tmp);
			updatePre('#vor,'+ minsObj[tmp]);
		}else if(minutes<=10){
			updatePre('#nach,'+ minsObj[Math.abs(minutes)]);
		}else{
			updatePre('');
		}
	}
	
	if(minsSecs >= 600 && minsSecs < 1200){
		tmp = 15-minutes;
		if(tmp<0){
			updatePre('#viertel, #nach'+','+ minsObj[Math.abs(tmp)]);
		}else if(tmp>0){
			updatePre('#viertel, #vor'+','+ minsObj[Math.abs(tmp)]);
		}else{
			updatePre('#viertel');
		}
	}
	
	if(minsSecs >= 1200 && minsSecs < 2400){
		tmp = 30 -minutes;
		if( tmp<0 ){
			updatePre('#nach, #halb, '+ minsObj[Math.abs(tmp)]);
		}else if(tmp>0){
			updatePre('#vor, #halb, '+ minsObj[Math.abs(tmp)]);
		}else{
			updatePre('#halb');
		}
	}
	
	if(minsSecs >= 2400 && minsSecs < 3000){
		tmp = 45-minutes;
		if(tmp<0){
			updatePre('#dreiviertel,#viertel, #nach'+','+ minsObj[Math.abs(tmp)]);
		}else if(tmp>=0){
			updatePre('#dreiviertel,#viertel, #vor'+','+minsObj[Math.abs(tmp)]);
		}else{
			updatePre('#dreiviertel, #viertel');
		}
	}
	
}

function updatePre(classes) {
    $('.pre').removeClass('active');
    $(classes).addClass('active');
}

function updateHour(classes) {
    $('.last').removeClass('active');
    $(classes).addClass('active');
}

setInterval(function() {
    swabianClock();
	//console.log('Update');
}, 1000);

swabianClock();
